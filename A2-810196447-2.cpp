#include <bits/stdc++.h>
using namespace std;

vector<string> makeCode(int numberOfBits)
{
    if(!numberOfBits)
    {
        vector<string> result;
        result.push_back("");
        return result;
    }
    vector<string> result, previous;
    previous = makeCode(numberOfBits-1);
    for(unsigned int i = 0; i < previous.size() ; i++)
        result.push_back('0' + previous[i]);
    for(unsigned int i = 0; i < previous.size() ; i++)
        result.push_back('1' + previous[previous.size() - i - 1]);
    return result;
}
int main()
{
    int numberOfBits;
    cin >> numberOfBits;
    vector<string> GrayCode = makeCode(numberOfBits);
    for(unsigned int i = 0; i < GrayCode.size() ; i++)
        cout << GrayCode[i] << '\n';
    return 0;
}
