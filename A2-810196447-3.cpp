#include <bits/stdc++.h>
using namespace std;

#define START_POINT 'S'
#define END_POINT 'E'
#define OBSTACLE '#'
#define VISITED 'V'
#define RIGHT "R"
#define LEFT "L"
#define UP "U"
#define DOWN "D"
bool isOk(int x, int y, int maxX, int maxY)
{
    if(x < 0 || y < 0 || x == maxX || y == maxY)
        return 0;
    return 1;
}
string Path(int x, int y, vector<string>& grid)
{
    if(grid[x][y] == END_POINT)
        return "";

    grid[x][y] = VISITED;

    int dx[] = {0, -1, 0, 1};
    int dy[] = {1, 0, -1, 0};
    string directions[] = {RIGHT, UP, LEFT, DOWN};

    vector<string> results;
    for(int i = 0; i < 4 ; i++)
        if(isOk(x + dx[i], y + dy[i], grid.size(), grid[0].size()) )
        {
            if(grid[x + dx[i]][y + dy[i]] != OBSTACLE && grid[x + dx[i]][y + dy[i]] != VISITED)
                results.push_back(directions[i] + Path(x + dx[i], y + dy[i], grid));
        }

    for(unsigned int i = 0; i < results.size(); i++)
        if(results[i].find("NO SOLUTION") == string::npos)
            return results[i];
    return "NO SOLUTION";
}

int main()
{
    vector<string> grid;
    string temp;
    while(getline(cin, temp))
        grid.push_back(temp);

    for(int i=0; i<grid.size(); i++)
        for(int j=0 ;j<grid[i].size();j++)
            if(grid[i][j] == 'S')
                 cout << Path(i, j, grid) << '\n' ;
    return 0;
}
