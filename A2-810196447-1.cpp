#include <bits/stdc++.h>
using namespace std;

int calculateTime(int targetClass, vector<vector<int> > prerequisite, vector<int> courseTime)
{
    int result = 0;
    for(unsigned int i = 0; i < prerequisite[targetClass].size(); i++)
        result = max(calculateTime(prerequisite[targetClass][i], prerequisite, courseTime), result);
    return result + courseTime[targetClass];
}

int main()
{
    vector<int> courseTime;
    int targetClass, numberOfCourses, numberOfRelations;
    cin >> numberOfCourses >> numberOfRelations >> targetClass;
    vector<vector<int> > prerequisite(numberOfCourses);
    for(int i = 0 ; i < numberOfCourses ; i++)
    {
        int temp;
        cin>>temp;
        courseTime.push_back(temp);
    }
    for(int i = 0 ; i < numberOfRelations ; i++)
    {
        int v, u;
        cin >> v >> u;
        prerequisite[u].push_back(v);
    }
    cout << calculateTime(targetClass, prerequisite, courseTime) << '\n';
    return 0;
}
