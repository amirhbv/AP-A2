#include <bits/stdc++.h>
using namespace std;

long long int f(vector<int> v, int n)
{
    if(n == 1)
        return v[0];
    else
        return f(v, n-1) + (n%2 ? v[n-1]:(-1) * v[n-1] );

}
int main()
{
    vector<int> v;
    int n;
    cin >> n;
    for(int i=0; i<n ;i++)
    {
        int temp;
        cin>>temp;
        v.push_back(temp);
    }
    cout << f(v, n) << '\n';
    return 0;
}
